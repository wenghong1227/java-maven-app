def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t wh1227/demo-app:jma-4.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push wh1227/demo-app:jma-4.0'
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this
